//void Web_updateSerial()
//{
//    //delay(500);
//    while (Serial.available())
//    {
//        Serial2.write(Serial.read());//Forward what Serial received to Software Serial Port
//    }
//    while (Serial2.available())
//    {
//        Serial.write(Serial2.read());//Forward what Software Serial received to Serial Port
//    }
//}
void Web_serial_read() {
    if (!Serial.available()) return;

    char currSymb = Serial.read();

    //Serial.print("Recieve char:");
    //Serial.println(currSymb);
    if ('\r' == currSymb)
    {
        Serial.println("Web: Find command:");

        // Выключение климат-контроля дома
        if (!Web_recieveMessage.compareTo("security off")) {
            SecuritySystem_Enabled = false;
            Serial.println("Result: security off");
        } 
        // Включение климат-контроля дома
        if (!Web_recieveMessage.compareTo("security on")) {
            SecuritySystem_Enabled = true;
            Serial.println("Result: security on");
        } 

        //Получение статуса охранной системы
        if (!Web_recieveMessage.compareTo("security status")) {
            if (SecuritySystem_Enabled)
            {
                Serial.println("Result: Security system is on");
            }
            else
            {
                Serial.println("Result: Security system is off");
            }
        }
        // Выключение климат-контроля 
        Web_recieveMessage = "";
    }
    else if ('\n' != currSymb) { Web_recieveMessage += String(currSymb); }




}