bool CheckMotion() {
    if (L1_Kitchen_Door_Sensor_Check())
    {
        return true;
    }
    if (L1_Hall_Door_Sensor_Check())
    {
        return true;
    }
    //if (L1_Room_Motions_Sensor_Check())
    //{
    //    return true;
    //}

    return false;
}
//Функция проверки датчика контроля двери на Первый этаж.Кухня
bool L1_Room_Motions_Sensor_Check() {
    int val = digitalRead(L1_Room_Motions_Sensor_Pin); // Если HIGH, то значит есть движение
    if (val == HIGH) {
        Serial.println("Есть движение!");
        return true;
    }
    else {
        Serial.println("Все тихо...");
        return false;
    }

}

//Функция проверки датчика контроля двери на Первый этаж.Коридор
bool L1_Hall_Door_Sensor_Check() {
    int val = digitalRead(L1_Hall_Door_Sensor_Pin); // читать Door_Sensor_Pin
    if (val == HIGH) { // Если Door_Sensor N.C. (без магнита) -> HIGH : Дверь открыта / LOW : Дверь закрыта
    // Если Door_Sensor N.0. (nc с магнитом) -> HIGH : Дверь открыта / LOW : Дверь закрыта
        //
        //digitalWrite(ledPin_blue, LOW); //выключить светодиод Door_Led
        return false;
    }
    else {
        //digitalWrite(ledPin_blue, HIGH); //включить светодиод Door_Led
        //playMelody();
        return true;
    }
}

bool L1_Kitchen_Door_Sensor_Check() {
    int val = digitalRead(L1_Kitchen_Door_Sensor_Pin); // читать Door_Sensor_Pin
    if (val == HIGH) { // Если Door_Sensor N.C. (без магнита) -> HIGH : Дверь открыта / LOW : Дверь закрыта
    // Если Door_Sensor N.0. (nc с магнитом) -> HIGH : Дверь открыта / LOW : Дверь закрыта

        return false;
    }
    else {

        return true;
    }
}