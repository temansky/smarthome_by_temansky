void GSM_init()
{
    delay(500);
    Serial2.println(F("AT+CMGF=1")); // Configuring TEXT mode
    delay(500);
    Serial2.println(F("AT+CPMS=SM,SM,SM"));
    delay(500);
    Serial2.println(F("AT+CMGD=1"));  //Очищаем накопившиеся СМС
    delay(500);
    Serial2.println(F("AT+CMGD=2"));  //Очищаем накопившиеся СМС
    delay(500);
    Serial2.println(F("AT+CMGD=3"));  //Очищаем накопившиеся СМС 
    delay(500);
    Serial2.println(F("AT+CMGD=4"));  //Очищаем накопившиеся СМС 
    delay(500);
    Serial2.println(F("AT+CMGD=5"));  //Очищаем накопившиеся СМС 
    delay(500);
    Serial2.println(F("AT+CNMI=2,2,0,0,0")); //включает оповещение о новых сообщениях
    delay(500);
}
//
void GSM_updateSerial()
{
    delay(300);
    while (Serial.available())
    {
        Serial2.write(Serial.read());//Forward what Serial received to Software Serial Port
    }
    while (Serial2.available())
    {
        Serial.write(Serial2.read());//Forward what Software Serial received to Serial Port
    }
}
void GSM_sms_read() {
    if (!Serial2.available()) return;

    char currSymb = Serial2.read();

    //Serial.print("Recieve char:");
    //Serial.println(currSymb);
    if ('\r' == currSymb)
    {
        //Serial.println("Found ending symbol");
        if (isStringMessage != 0)
        {
            Serial.print("GSM: Find command: "); Serial.println(GSM_recieveMessage);
            //если текущая строка - SMS-сообщение,
            //отреагируем на него соответствующим образом

            // Включение климат-контроля дома
            if (!GSM_recieveMessage.compareTo("Security off")) {
                SecuritySystem_Enabled = false;

                //Отправляем СМС-отчет
                Serial.println("Result: security off");
                ClearSmsMemory();
            } 

            // Выключение климат-контроля дома
            if (!GSM_recieveMessage.compareTo("Security on")) {
                SecuritySystem_Enabled = true;

                //Отправляем СМС-отчет
                ClearSmsMemory();
            } 

            //Получение статуса охранной системы
            if (!GSM_recieveMessage.compareTo("Security status")) {
                if (SecuritySystem_Enabled)
                {
                    //Отправляем смс хозяину
                    SendSms("Result: Security system is on");
                    Serial.println("Send msg to housekeeper: Result: Security system is on");
                    Serial.println("Result: Security system is on");
                }
                else
                {
                    //Отправляем смс хозяину
                    SendSms("Result: Security system is off");
                    Serial.println("Send msg to housekeeper: Result: Result: Security system is off");
                    Serial.println("Result: Security system is off");
                }
                ClearSmsMemory();
            }


            //else if (!GSM_recieveMessage.compareTo("BALANCE")) { SMSbalance(); } // Запрос баланса

            isStringMessage = 0;
        }
        else {
            Serial.print("I've recieved article: "); Serial.println(GSM_recieveMessage);

            if (GSM_recieveMessage.startsWith("CMD")) 
            {
                isStringMessage = 1; 
                Serial.println("I've found CMD article");

            }
            if (GSM_recieveMessage.startsWith("+CMT: \"+79967729284"))
            {
                Serial.println("This sms from housekeeper");
                isStringMessage = 1; 
                //KTOzapros = 1;
            } //если строка начинается с "+CMT..тел 1 абонента",то след. строка является сообщ от аб.1

            //if (GSM_recieveMessage.startsWith("+CMT: \"+79*********")) { isStringMessage = 2; KTOzapros = 2; } //если строка начинается с "+CMT..тел 2 абонента",то след. строка является сообщ от аб.2
            //if (GSM_recieveMessage.startsWith("+CUSD: 0,"))  //если текущая строка начинается с "+CUSD",то следующая строка является запросом баланса
            //{
            //    if (KTOzapros != 0) {              // если запрос не по блютуз, шлём смс тому, кто запросил.
            //        if (KTOzapros == 1) { startOneSMS(); }
            //        if (KTOzapros == 2) { startTwoSMS(); }
            //        Serial2.print(GSM_recieveMessage);  // пересылка содержимого смс-ки баланса абоненту
            //        EndSMS();
            //    }
            //    else Serial1.print(GSM_recieveMessage);      // если запрос с WEB-интерфейса , шлём баланс туда
            //}
        }
        GSM_recieveMessage = "";
    }
    else if ('\n' != currSymb) { GSM_recieveMessage += String(currSymb); }
}



void SendSms(String message) {
    Serial2.println("AT+CMGF=1"); // Configuring TEXT mode
    delay(500);
    Serial2.println("AT+CMGS=\"+79967729284\"");//change ZZ with country code and xxxxxxxxxxx with phone number to sms
    delay(500);
    Serial2.print(message); //text content
    delay(500);
    Serial2.write(26);
}
void ClearSmsMemory() {
    GSM_init();


}