#include "pitches.h"
const String version = "0.18b";
unsigned long timing; //Счетчик времени
//uint32_t period = 5 * 60000L;// 5 minutes
uint32_t period = 30000L;// 5 minutes


//_______________SECURITY_SYSTEM_____________
bool SecuritySystem_Enabled = false;
bool SecuritySystem_Alert = false;


//_________Подключение дисплея LCD по I2C_________________________
//#define __PROG_TYPES_COMPAT__
#include <Wire.h> 
#include <LiquidCrystal_I2C_OLED.h>
          LiquidCrystal_I2C OLED1(0x27,20,4);  // адресс LCD по I2C шине = 0x27

//**************************************************************************************


//_________________________Часы RTC DS3231_________________________
//http://mynobook.blogspot.ru/2016/05/ds3231.html
#include <DS3231.h>          // библиотека работы с часами
DS3231  rtc(SDA, SCL);       // Часы на подключаются к SDA и SCL

Time t;
Time tempTime; //Переменная для периодического обновления экрана
int RTCSekunda;              // Переменная "секунда" для суточного таймера цикл RTCtimer()
int RTCChas;                 // Переменная "час"     для суточного таймера цикл RTCtimer()
int RTCMinuta;               // Переменная "минута"  для суточного таймера цикл RTC_timer()
int RTCDay;                  // Переменная "день"    для суточного таймера цикл RTC_timer()
int RTCdate;                 // Переменная "число"   для суточного таймера цикл RTC_timer()
int RTCmon;                  // Переменная "месяц"   для суточного таймера цикл RTC_timer()
int RTCyear;                 // Переменная "год"     для суточного таймера цикл RTC_timer()
int statusBud=2;             // Переменная "будильник" 0 - ожидание, 1 - прозвучал в этих сутках, 2 - отключен
bool NewDay=false;        // Флаг "новые сутки" для функции автокорректировки хода часов
bool statusKur=false;  

//_________________________DHT22_________________________
#include "DHT.h"
//Конфигурация датчиков температуры
//Датчик подвала
DHT L1_Vault_dht(8, DHT22);
float L1_Vault_dht_h;
float L1_Vault_dht_t;

DHT L1_Hall_dht(4, DHT11);
float L1_Hall_dht_h;
float L1_Hall_dht_t;

//______________________OneWire______________________
#include <OneWire.h>
OneWire ds(50); // датчики DS18B20 на 50 пин
byte Street_ds18b21_address[8] = { 0x28, 0x03, 0xC1, 0x3B, 0x04, 0x00, 0x00, 0xEA }; // Сетевой адрес датчика DS18B20 в зале 28 3 C1 3B 4 0 0 EA
float  Street_ds18b21_t;


//**************************************************************************************

//_________________________Константы для кнопок_________________________
// задаем константы
const int buttonUp = 2;// номер входа, подключенный к кнопке
int buttonUp_flag = 0; //Флаг для защиты от дребезга

const int buttonDown = 3;     // номер входа, подключенный к кнопке
int buttonDown_flag = 0; //Флаг для защиты от дребезга

const int ledPin_red = 9;      // номер выхода светодиода
const int ledPin_green = 10;      // номер выхода светодиода
const int ledPin_blue = 11;      // номер выхода светодиода

const int L1_Kitchen_Door_Sensor_Pin = 22; // контакт для датчика двери 
const int L1_Hall_Door_Sensor_Pin = 24; // контакт для датчика двери
const int L1_Room_Motions_Sensor_Pin = 48; // контакт для движения



const int L1_Hall_Buzzer_Pin = 5; // контакт для сирены


void Init_PIN() { //Функция инициализации пинов на плате
    pinMode(ledPin_red, OUTPUT);  //  // инициализируем пин, подключенный к светодиоду, как выход
    pinMode(ledPin_green, OUTPUT);  //  // инициализируем пин, подключенный к светодиоду, как выход
    pinMode(ledPin_blue, OUTPUT);  //  // инициализируем пин, подключенный к светодиоду, как выход

    pinMode(L1_Hall_Buzzer_Pin, OUTPUT);  //  // инициализируем пин, подключенный к светодиоду, как выход

    pinMode(buttonUp, INPUT);  // инициализируем пин, подключенный к кнопке, как вход
    pinMode(buttonDown, INPUT);  // инициализируем пин, подключенный к кнопке, как вход

    pinMode(L1_Kitchen_Door_Sensor_Pin, INPUT); // установить L1_Kitchen_Door_Sensor_Pin как вход
    pinMode(L1_Hall_Door_Sensor_Pin, INPUT); // установить L1_Hall_Door_Sensor_Pin как вход
    pinMode(L1_Room_Motions_Sensor_Pin, INPUT); // установить L1_Hall_Buzzer_Pin как вход

}

//____________________KEYPAD______________
#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 4;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', '4'},
  {'5', '6', '7', '8'},
  {'7', '8', '9', 'A'},
  {'B', 'C', 'D', 'E'}
};

byte rowPins[ROWS] = { 30, 31, 32, 33 };
byte colPins[COLS] = { 34, 35, 36, 37 };

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);



//____________________GSM______________
String GSM_recieveMessage="";
int isStringMessage = 0;      // 0 - не сообщение, 1 - сообщение от абонента №1, 2 - сообщение от абонента №2

//____________________WEB______________
String Web_recieveMessage = "";



//___________________ Стартовый цикл _________________________________
void setup()
{
    //Инициализация порта 
    Serial.begin(9600);

    //Инициализация UART с GSM-модулем
    Serial2.begin(115200);


    Init_PIN();  //Инициализируем пины на плате
    GSM_init(); //Инициализируем GSM-плату

    rtc.begin();   // инициализация часов

    InitTemperatureSensor();//Инициализируем датчики температуры

    OLED1.init();               // инициализация ЖК дисплея      
    OLED1.clear();
    OLED1.backlight();
    delay(1000);
    Display_SetPage_main();  //Выводим на экран главную страницу


    CheckValueTemperatureSensors();//Считываем показания датчиков температуры
}


//___________________ Рабочий цикл _________________________________
void loop()
{
    Display_GetButtonsState();
    CheckKeypad();

    SecuritySystem();

    GSM_sms_read();
    //GSM_updateSerial();
    //Web_serial_read();

    //Обновление экрана раз в минутуs
    tempTime = rtc.getTime();//Получаем текущее время и, 
    if ((millis() - timing) > period) //Каждый тик сравниваем текущее время и, если прошло Period секунд, то делаем определенное действие
    {
        Display_Refresh_Page();
        timing = millis();//Обновляем счетчик секунд
        CheckValueTemperatureSensors();
        Display_ChangePageDisplay("refresh");
    }

}
