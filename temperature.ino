void InitTemperatureSensor() {
    L1_Vault_dht.begin();//Инициализируем датчика DHT22
    L1_Hall_dht.begin();
}

void CheckValueTemperatureSensors() {
    CheckValue_L1_Vault_dht();
    CheckValue_L1_Hall_dht();

    Check_DallasSensors();
}

void CheckValue_L1_Hall_dht(){
    L1_Hall_dht_h = L1_Hall_dht.readHumidity();//Измеряем влажность
    L1_Hall_dht_t = L1_Hall_dht.readTemperature(); //Измеряем температуру

    if (isnan(L1_Hall_dht_h))  {   // Проверка. Если не удается считать показания, то пытаемся еще раз считать показания
        L1_Hall_dht_h = L1_Hall_dht.readHumidity();
    }
    if (isnan(L1_Hall_dht_t)) {  // Проверка. Если не удается считать показания, то пытаемся еще раз считать показания
        L1_Hall_dht_t = L1_Hall_dht.readTemperature();
    }
}
void CheckValue_L1_Vault_dht() {
    L1_Vault_dht_h = L1_Vault_dht.readHumidity(); //Измеряем влажность
    L1_Vault_dht_t = L1_Vault_dht.readTemperature(); //Измеряем температуру

    if (isnan(L1_Vault_dht_h)) {  // Проверка. Если не удается считать показания, то пытаемся еще раз считать показания
        L1_Vault_dht_h = L1_Vault_dht.readHumidity();
    }
    if (isnan(L1_Vault_dht_t)) {  // Проверка. Если не удается считать показания, то пытаемся еще раз считать показания
        L1_Vault_dht_t = L1_Vault_dht.readTemperature();
    }
}

void Check_DallasSensors() {
    ds.reset();    // сброс шины
    ds.write(0xCC);  // обращение ко всем датчикам



    CheckValue_Street_ds18b21();
}
void CheckValue_Street_ds18b21(){    
    Street_ds18b21_t = GetTemperatureFrom_ds18(Street_ds18b21_address);
}

float GetTemperatureFrom_ds18(byte addr[8]) {
    byte i;
    byte present = 0;
    byte type_s;
    byte data[12];
    float celsius, fahrenheit;

    if(OneWire::crc8(addr, 7) != addr[7]) {
        Serial.println("CRC is not valid!");  //  "CRC не корректен!"
        return;
    }
    Serial.println();

    // первый ROM-байт, определяет, с каким чипом мы имеем дело:
    switch (addr[0]) {
    case 0x10:
        Serial.println("  Chip = DS18S20");  //  "  Чип – DS18S20" или старый DS1820
        type_s = 1;
        break;
    case 0x28:
        Serial.println("  Chip = DS18B20");  //  "  Чип – DS18B20"
        type_s = 0;
        break;
    case 0x22:
        Serial.println("  Chip = DS1822");  //  "  Чип – DS1822"
        type_s = 0;
        break;
    default:
        Serial.println("Device is not a DS18x20 family device.");  //  "Устройство не принадлежит семейству DS18x20."
        return;
    }

    ds.reset();
    ds.select(addr);
    ds.write(0x44, 1);        // запускаем конверсию и включаем паразитное питание

    delay(1000);     // 750 миллисекунд может хватить, а может и нет;
                     // здесь можно использовать ds.depower(), 
                     // но об этом позаботится сброс 

    present = ds.reset();
    ds.select(addr);
    ds.write(0xBE);         // считываем scratchpad-память

    Serial.print("  Data = ");  //  "  Данные = "
    Serial.print(present, HEX);
    Serial.print(" ");
    for (i = 0; i < 9; i++) {           // нам нужно 9 байтов
        data[i] = ds.read();
        Serial.print(data[i], HEX);
        Serial.print(" ");
    }
    Serial.print(" CRC=");
    Serial.print(OneWire::crc8(data, 8), HEX);
    Serial.println();

    // конвертируем данные в температуру; поскольку результат - это
    // 16-битное целое знаковое число, оно должно быть записано в типе 
    // данных "int16_t", который всегда будет 16-битным – даже если
    // данные скомпилированы на 32-битном процессоре.
    int16_t raw = (data[1] << 8) | data[0];
    if (type_s) {
        raw = raw << 3; // разрешение по умолчанию – 9 бит
        if (data[7] == 0x10) {
            // регистр "count remain" дает полное 12-битное разрешение
            raw = (raw & 0xFFF0) + 12 - data[6];
        }
    }
    else {
        byte cfg = (data[4] & 0x60);
        // при низком разрешении младшие биты не определяются, поэтому обнуляем их:
        if (cfg == 0x00) raw = raw & ~7;  // 9-битное разрешение (93,75 миллисекунд)
        else if (cfg == 0x20) raw = raw & ~3;  // 10-битное разрешение (187,5 миллисекунд)
        else if (cfg == 0x40) raw = raw & ~1;  // 11-битное разрешение (375 миллисекунд)

        //// по умолчанию стоит 12-битное разрешение; время конверсии – 750 миллисекунд
    }
    celsius = (float)raw / 16.0;
    fahrenheit = celsius * 1.8 + 32.0;
    Serial.print("  Temperature = ");  //  "Температура = "
    Serial.print(celsius);
    Serial.print(" Celsius, ");  //  " по Цельсию, "
    Serial.print(fahrenheit);
    Serial.println(" Fahrenheit");  //  " по Фаренгейту"

    return celsius;
}
