//_________ Переменные для работы экрана_________________________
int Display_currentPage = 0; //Переменная текущей страницы
const int Display_maxPages = 11; //Максимальное количество страниц




void Display_GetButtonsState() {//Обработчик нажатия кнопок
    //Переменная flag позволяет избавиться от дребезга контактов=
    //Кнопка вверх
    // проверяем нажата ли кнопка если нажата, то buttonState будет HIGH:
    if (digitalRead(buttonUp) == HIGH && buttonUp_flag == 0) {//если кнопка нажата  
        buttonUp_flag = 1;
        Display_ChangePageDisplay("next");
    }
    if (digitalRead(buttonUp) == LOW && buttonUp_flag == 1)//если кнопка НЕ нажата
    {
        buttonUp_flag = 0;//обнуляем переменную flag
    }


    //Кнопка вниз
    // проверяем нажата ли кнопка если нажата, то buttonState будет HIGH:
    if (digitalRead(buttonDown) == HIGH && buttonDown_flag == 0) {//если кнопка нажата  
        buttonDown_flag = 1;
        Display_ChangePageDisplay("previous");
    }
    if (digitalRead(buttonDown) == LOW && buttonDown_flag == 1)//если кнопка НЕ нажата и переменная flag равна - 1 ,то ...
    {
        buttonDown_flag = 0;//обнуляем переменную flag
    }




}

void Display_ChangePageDisplay(String key)
{
    //В зависимости от того, какая была нажата клавиша, выбираем следующую или предыдущую страницу
    if (key == "next")//
        Display_currentPage++;
    else if (key == "previous")
        Display_currentPage--;

    //В зависимости от того, какая текущая страница, запускаем функцию для отображения необходимой страницы
    switch (Display_currentPage) {
    case 0:
        Display_SetPage_main();
        break;
    case 1:
        Display_SetPage_Street();
        break;
    case 2:
        Display_L1_Toilet();
        break;
    case 3:
        Display_L1_Room();
        break;
    case 4:
        Display_L1_Kitchen();
        break;
    case 5:
        Display_L1_Hall();
        break;
    case 6:
        Display_L1_Vault();
        break;
    case 7:
        Display_L2_Storage1();
        break;
    case 8:
        Display_L2_Storage2();
        break;
    case 9:
        Display_L2_Room();
        break;
    case 10:
        Display_L2_Cabinet();
        break;
    case Display_maxPages://Для бесконечной прокрутки страниц
        Display_currentPage = 0;
        Display_SetPage_main();
        break;
    case -1://Для бесконечной прокрутки страниц
        Display_currentPage = 10;
        Display_L2_Cabinet();
        break;
    default:
        break;
    }

    Serial.print("Current Display_currentPage:"); Serial.println(Display_currentPage);
}
//_______Перечисление страниц
void Display_SetPage_main() {//Главная страница
    t = rtc.getTime();
    OLED1.clear();
    //1-я строка
    OLED1.setCursor(0, 0);  OLED1.print("Date:");
    OLED1.setCursor(7, 0);  OLED1.print(t.date, DEC);
    OLED1.setCursor(9, 0);  OLED1.print(".");
    OLED1.setCursor(10, 0); OLED1.print(t.mon, DEC);
    OLED1.setCursor(12, 0); OLED1.print(".");
    OLED1.setCursor(13, 0); OLED1.print(t.year, DEC);

    //2-я строка
    OLED1.setCursor(0, 1);  OLED1.print("Time:");
    if (RTCChas < 10) {
        OLED1.setCursor(7, 1);
        OLED1.print(t.hour, DEC);
    }
    else {
        OLED1.setCursor(7, 1);
        OLED1.print(t.hour, DEC);
    }
    OLED1.setCursor(9, 1); OLED1.print(":");
    OLED1.setCursor(10, 1); RTCMinuta = t.min;
    if (RTCMinuta < 10) {
        OLED1.print("0");
        OLED1.setCursor(11, 1);
        OLED1.print(t.min, DEC);
    }
    else {
        OLED1.setCursor(10, 1);
        OLED1.print(t.min, DEC);
    }
    //3-я строка
    OLED1.setCursor(0, 2); OLED1.print("Day:"); OLED1.setCursor(7, 2);
    switch (t.dow) {
    case  1: OLED1.print("Monday");  break;
    case  2: OLED1.print("Tuesday");      break;
    case  3: OLED1.print("Wednesday");        break;
    case  4: OLED1.print("Thursday");      break;
    case  5: OLED1.print("Friday");      break;
    case  6: OLED1.print("Saturday");      break;
    case  7: OLED1.print("Sunday");  break;
    }
    OLED1.setCursor(0, 3);  OLED1.print("Version:"); OLED1.print(version);
    Display_PrintCountPage();
}

void Display_SetPage_Street() //Страница улицы
{
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(Street_ds18b21_t);



    Display_PrintTitle("Street",""); 
    Display_PrintCountPage();
}

void Display_L1_Toilet() {//Страница Первый этаж.Санузел
    OLED1.clear();


    Display_PrintTitle("L1", "Toilet");
    Display_PrintCountPage();
}
void Display_L1_Room() {//Страница Первый этаж.Комната
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(rtc.getTemp());

    Display_PrintTitle("L1", "Room");
    Display_PrintCountPage();
}
void Display_L1_Kitchen() {//Страница Первый этаж.Кухня
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(rtc.getTemp());

    Display_PrintTitle("L1", "Kitchen");
    Display_PrintCountPage();
}
void Display_L1_Hall() {//Страница Первый этаж.Коридор
    OLED1.clear();
    //Показания температуры
    if (isnan(L1_Hall_dht_t)) // Проверка. Если не удается считать показания, то пишем ошибку
    {
        OLED1.setCursor(0, 0);  OLED1.print("Temp: Error");
    }
    else
    {
        OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(L1_Hall_dht_t);
    }

    //Показания влажности
    if (isnan(L1_Hall_dht_h))// Проверка. Если не удается считать показания, то пишем ошибку
    {

        OLED1.setCursor(0, 1);  OLED1.print("Hum: Error");
    }
    else
    {
        OLED1.setCursor(0, 1);  OLED1.print("Hum: "); OLED1.print(L1_Hall_dht_h);
    }

    Display_PrintTitle("L1", "Hall");
    Display_PrintCountPage();
}
void Display_L1_Vault() {//Страница Первый этаж.Подвал

    OLED1.clear();
    //Показания температуры
    if (isnan(L1_Vault_dht_t)) // Проверка. Если не удается считать показания, то пишем ошибку
    {
        OLED1.setCursor(0, 0);  OLED1.print("Temp: Error"); 
    }
    else
    {
        OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(L1_Vault_dht_t);
    }

    //Показания влажности
    if (isnan(L1_Vault_dht_h))// Проверка. Если не удается считать показания, то пишем ошибку
    {

        OLED1.setCursor(0, 1);  OLED1.print("Hum: Error");
    }
    else
    {
        OLED1.setCursor(0, 1);  OLED1.print("Hum: "); OLED1.print(L1_Vault_dht_h);
    }

    Display_PrintTitle("L1", "Vault");
    Display_PrintCountPage();
}

void Display_L2_Storage1() {//Страница Второй этаж.Кладовка1
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(rtc.getTemp());

    Display_PrintTitle("L2", "Storage1");
    Display_PrintCountPage();
}
void Display_L2_Storage2() {//Страница Второй этаж.Кладовка2
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(rtc.getTemp());

    Display_PrintTitle("L2", "Storage2");
    Display_PrintCountPage();
}
void Display_L2_Room() {//Страница Второй этаж.Комната
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(rtc.getTemp());

    Display_PrintTitle("L2", "Room");
    Display_PrintCountPage();
}
void Display_L2_Cabinet() {//Страница Второй этаж.Кабинет
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Temp: "); OLED1.print(rtc.getTemp());

    Display_PrintTitle("L2", "Cabinet");
    Display_PrintCountPage();
}

void Display_PrintCountPage() {//Функция для вывода на экран текущей страницы

    //4-я строка
    if (Display_currentPage<10)
    {
        OLED1.setCursor(16, 3);  OLED1.print(Display_currentPage); OLED1.print("/"); OLED1.print(Display_maxPages - 1);
    }
    else
    {
        OLED1.setCursor(15, 3);  OLED1.print(Display_currentPage); OLED1.print("/"); OLED1.print(Display_maxPages - 1);
    }
    

}
void Display_PrintTitle(String location, String room)//Функция печати заголовка
{
    //4-я строка
    OLED1.setCursor(0, 3);  OLED1.print(location); OLED1.print(" "); OLED1.print(room);
}
void Display_Refresh_Page() {//Страница Второй этаж.Кабинет
    OLED1.clear();
    OLED1.setCursor(0, 0);  OLED1.print("Waiting..."); 
}